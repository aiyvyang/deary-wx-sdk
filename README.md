微信官方 JS-SDK
----

说明: 仅将官方 js-sdk 发布到 npm，支持使用 AMD/CMD 标准模块加载方法加载，便于 vite，browserify，webpack 等直接使用。


官方 JS 源码: https://res.wx.qq.com/open/js/jweixin-1.6.0.js

官方使用说明: https://developers.weixin.qq.com/doc/offiaccount/OA_Web_Apps/JS-SDK.html

安装:
```shell
pnpm add @deary/wx-sdk
```

or

```shell
npm install @deary/wx-sdk
```

使用:

```javascript
// commonjs
const wx = require('@deary/wx-sdk');

// es module
import wx from '@deary/wx-sdk'
```
